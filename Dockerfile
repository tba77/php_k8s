FROM php:7.3-fpm
RUN docker-php-ext-install mysqli pdo pdo_mysql gettext mbstring exif fileinfo
RUN mkdir /app
COPY ./app/index.php /app
